package org.ashish;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseQueries {

	
	private static Connection getRemoteConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Properties dbProps = loadDBProperties();
			String dbName = dbProps.getProperty("RDS_DB_NAME");
			String userName = dbProps.getProperty("RDS_USERNAME");
			String password = dbProps.getProperty("RDS_PASSWORD");
			String hostname = dbProps.getProperty("RDS_HOSTNAME");
			String port = dbProps.getProperty("RDS_PORT");
			String jdbcUrl = "jdbc:mysql://" + hostname + ":" + port + "/" + dbName + "?user=" + userName + "&password=" + password;
			System.out.println(""+ jdbcUrl);
			Connection con = DriverManager.getConnection(jdbcUrl);
			System.out.println("Remote connection successful.");
			return con;
		}
		catch (Exception e) { 
			System.err.println(e.toString());
		}
		return null;
	}

	private static Properties loadDBProperties() throws Exception{
		String resourceName = "db_info.properties"; // could also be a constant
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
		try(InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
			props.load(resourceStream);
			return props;
		} catch (IOException e1) {
			e1.printStackTrace();
			throw new Exception(e1);
		}
	}


	public UserObj getUserDataFromDB(String emailId){
		Connection conn=null;
		UserObj user = null;
		try{
			conn = getRemoteConnection();
			PreparedStatement statement = conn.prepareStatement("select * from user_records where email_id = ?");
			statement.setString(1, emailId);    
			ResultSet resultSet = statement.executeQuery();
			statement.clearParameters();
			if(resultSet.next()) {
				user = new UserObj();
				user.setEmailId(resultSet.getString(2));
				user.setFirstName(resultSet.getString(3));
				user.setLastName(resultSet.getString(4));
				user.setContactNum(resultSet.getString(5));
				user.setDesignation(resultSet.getString(6));
				user.setResumeMetaId(resultSet.getString(7));
				user.setProfileMetaId(resultSet.getString(8));
				System.out.println("print data");
			}
			resultSet.close();
			statement.close();
			conn.close();
			return user;
		} catch (SQLException ex) {
			// Handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			System.out.println("Closing the connection.");
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return user;
	}


}
